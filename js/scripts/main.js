import Responsive from './Responsive';
const topSlide = require('./topSlide.js');

/* ===============================================
check browser
=============================================== */
const bowser = require('bowser');
let body_class = '';
if(bowser.msie === true) {
	body_class = 'ie'
}else if(bowser.msedge === true){
	body_class = 'edge'
}else if(bowser.name == 'Chrome'){
	body_class = 'chrome'
}

(function($) {
$(function(){
	$('body').addClass(body_class);
});
})(jQuery);
/* ===============================================
Check responsive state
=============================================== */
const r = new Responsive();

$(window).bind('resize',function(){
	//check is responsive;

});
//check is mobile;


(function($) {
$(function(){
	$(window).on('load',function(){
		$(".regular").slick({
			autoplay:true,
			dots: true,
			infinite: true,
			slidesToShow: 1,
			centerMode: true, //要素を中央寄せ
			centerPadding: '96px'
		});
	});

});
})(jQuery);


