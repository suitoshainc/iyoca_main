window.$ = jQuery;
const bxSlider = require('../../lib/js/bxslider-4/jquery.bxSlider.min.js');

(function($) {
$(function(){
	let param = {
		speed: 1000,
		controls: true,
		pause: 4000,
		pager:true,
		auto: true,
		useCSS:false,
		onSliderLoad:function(){
			$('.slide').removeClass('onload');
		}
	};

	$('.slide ul').bxSlider(param);
});
})(jQuery);


